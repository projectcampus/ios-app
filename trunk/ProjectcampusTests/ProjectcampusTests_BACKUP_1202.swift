//
//  ProjectcampusTests.swift
//  ProjectcampusTests
//
//  Created by Jeroen Zonneveld on 14-04-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//


//INFO NA CONCULT RAYMOND
//self.waitForExpectationsWithTimeout
//een test voer je uit met een XCTAssert
//nshipster.com

/////////////////////////////////////////////////////////
//////////////////BEFORE STARTING TEST//////////////////
///////////////////////////////////////////////////////
/// - Start app                                     //
/// - Get valid token from console                 //
/// - add token to buttom constraint (let) token ///
///////////////////////////////////////////////////
//////////////////////////////////////////////////
/////////////////////////////////////////////////

//Since XCTestCase is not intended to be initialized directly from within a test case definition, shared properties initialized in setUp are declared as optional vars in Swift. As such, it's often much simpler to forgo setUp and assign default values

import UIKit
import XCTest
import Projectcampus
import Alamofire
import SwiftyJSON

class ProjectcampusTests: XCTestCase {
    
    var authorization:Authorisation = Authorisation.get // <-- Like here. forgo setup and assing default value
    let token = "MzFiM2NmODZkZmE4YzY5YTU1NDdkNGExOWJhNDI3ZDRkMDMyNmJiOTlmMTU5ODkxM2NmNWU2NzUyYTEwZWY1MA"
    var jsonProjects:JSON?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.authorization.setToken(self.token)
        convertLocalJsonToVar()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func convertLocalJsonToVar() {
        
        ////// BRON VOOR IMPORT LOCAL JSON:
        ///// http://interactivelogic.net/wp/2015/01/how-to-read-a-json-file-from-your-bundle-and-output-the-contents-as-a-string-in-swift/
        
        // import local file
        let filePath = NSBundle.mainBundle().pathForResource("project",ofType:"json")
        
        // optional varibale for setting an error
        var readError:NSError?
        
        // load data from file
        if let data = NSData(contentsOfFile:filePath!, options:NSDataReadingOptions.DataReadingUncached, error:&readError) {
            
            // put data in string
            let stringData = NSString(data: data, encoding: NSUTF8StringEncoding)
            
            if let dataNew = stringData! as? String {
            // put string in JSON varibale
<<<<<<< HEAD
            jsonProjects = JSON(rawValue: stringData!)
=======
            json = JSON(rawValue: dataNew)
            }
>>>>>>> 8739979cdc5cf0180e79e1fac5c92839eafd54ee
        }
    }
        
    
    

    //==================TESTCASE 1===================//
    
    func testForValidTokenCode() {
        
        //check if size is 86 chars long
        XCTAssertEqual(count(authorization.getToken()), 86, "The token should have a size of 86 characters")
        
        //check if token only contains a-z and 0-9
        var stringlength = count(authorization.getToken())
        var ierror: NSError?
        var regex:NSRegularExpression = NSRegularExpression(pattern: "([a-z0-9])", options: NSRegularExpressionOptions.CaseInsensitive, error: &ierror)!
        var modString = regex.stringByReplacingMatchesInString(self.token, options: nil, range: NSMakeRange(0, stringlength), withTemplate: "")
        XCTAssertEqual(modString, "", "The modString should be empty if the token only has a-z or 0-9 content")
    }
    
    //==================TESTCASE 1===================//
    func testCheckJSONToProject() {
        var projects = JSONToObjectConverter.get.ConvertToProjects(jsonProjects!)
        XCTAssertNotNil(projects, "Projects is empty")
        XCTAssertGreaterThanOrEqual(projects.count, 1, "The projects array should be greater then or equal 1")
    }
    
    //==================TESTCASE 2===================//
    func testCheckProjectTitles() {
<<<<<<< HEAD
       // for JSON jsonObject in json {
            
            
            
      //  }
=======
//        var projects = JSONToObjectConverter.get.ConvertToProjects(json!)
//        for project in projects {
//            project.name
//            
//        }
>>>>>>> 8739979cdc5cf0180e79e1fac5c92839eafd54ee
    }
    
    //==================TESTCASE 3===================//
    func testCheckJSONToProjectsFollow() {
        
    }
    
    //==================TESTCASE 4===================//
    func testCheckJSONToProjectsCoach() {
        
    }
    
    //==================TESTCASE 5===================//
    func testCheckJSONToWorkspaces() {
        
    }
}
