//
//  ProjectcampusTests.swift
//  ProjectcampusTests
//
//  Created by Jeroen Zonneveld on 14-04-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//


//INFO NA CONCULT RAYMOND
//self.waitForExpectationsWithTimeout
//een test voer je uit met een XCTAssert
//nshipster.com

/////////////////////////////////////////////////////////
//////////////////BEFORE STARTING TEST//////////////////
///////////////////////////////////////////////////////
/// - Start app                                     //
/// - Get valid token from console                 //
/// - add token to buttom constraint (let) token ///
///////////////////////////////////////////////////
//////////////////////////////////////////////////
/////////////////////////////////////////////////

//Since XCTestCase is not intended to be initialized directly from within a test case definition, shared properties initialized in setUp are declared as optional vars in Swift. As such, it's often much simpler to forgo setUp and assign default values

import UIKit
import XCTest
import Projectcampus
import Alamofire
import SwiftyJSON

class ProjectcampusTests: XCTestCase {
    
    var authorization:Authorisation = Authorisation.SharedInstance // <-- Like here. forgo setup and assing default value
    let token = "MzFiM2NmODZkZmE4YzY5YTU1NDdkNGExOWJhNDI3ZDRkMDMyNmJiOTlmMTU5ODkxM2NmNWU2NzUyYTEwZWY1MA"
    var jsonProjects:JSON?
    var jsonNotifications:JSON?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.authorization.setToken(self.token)
        convertLocalJsonToVar()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func convertLocalJsonToVar() {
        
        ////// BRON VOOR IMPORT LOCAL JSON:
        ///// http://interactivelogic.net/wp/2015/01/how-to-read-a-json-file-from-your-bundle-and-output-the-contents-as-a-string-in-swift/
        
        /////////////////////////
        //////// PROJECT  //////
        ///////////////////////
        
        // import local file
        let filePathProject = NSBundle.mainBundle().pathForResource("project",ofType:"json")
        
        // optional varibale for setting an error
        var readErrorProject:NSError?
        
        // load data from file
        if let data = NSData(contentsOfFile:filePathProject!, options:NSDataReadingOptions.DataReadingUncached, error:&readErrorProject) {
            
            //maak van input data een NSAarray
            let jsonData = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSArray
            
            //plaats json data als JSON format in jsonProjects
            jsonProjects = JSON(rawValue: jsonData)
        }
        
        /////////////////////////
        ////  Notification  ////
        ///////////////////////
        
        // import local file
        let filePathNotification = NSBundle.mainBundle().pathForResource("notification",ofType:"json")
        
        // optional varibale for setting an error
        var readErrorNotificiation:NSError?
        
        // load data from file
        if let data = NSData(contentsOfFile:filePathNotification!, options:NSDataReadingOptions.DataReadingUncached, error:&readErrorNotificiation) {
            
            //maak van input data een NSAarray
            let jsonDataNotifications = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSArray
            
            //plaats json data als JSON format in jsonProjects
            jsonNotifications = JSON(rawValue: jsonDataNotifications)
        }
    }
    
    
    //==================TESTCASE 1===================//
    
    func testForValidTokenCode() {
        
        //check if size is 86 chars long
        XCTAssertEqual(count(authorization.getToken()), 86, "The token should have a size of 86 characters")
        
        //check if token only contains a-z and 0-9
        var stringlength = count(authorization.getToken())
        var ierror: NSError?
        var regex:NSRegularExpression = NSRegularExpression(pattern: "([a-z0-9])", options: NSRegularExpressionOptions.CaseInsensitive, error: &ierror)!
        var modString = regex.stringByReplacingMatchesInString(self.token, options: nil, range: NSMakeRange(0, stringlength), withTemplate: "")
        XCTAssertEqual(modString, "", "The modString should be empty if the token only has a-z or 0-9 content")
    }
    
    //==================TESTCASE 1===================//
    func testCheckJSONToProject() {
        var projects = JSONToObjectConverter.SharedInstance.ConvertToProjects(jsonProjects!)
        XCTAssertNotNil(projects, "Projects is empty")
        XCTAssertGreaterThanOrEqual(projects.count, 1, "The projects array should be greater then or equal 1")
        
        //println(projects.count)
    }
    
    //==================TESTCASE 2===================//
    func testCheckProjectTitles() {
        var projects = JSONToObjectConverter.SharedInstance.ConvertToProjects(jsonProjects!)
        var characterSet:NSCharacterSet = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
       
        var searchTerm = [Character]()
        for project in projects {
            if (NSString(string: project.name!).rangeOfCharacterFromSet(characterSet.invertedSet).location == NSNotFound){
                println("No special characters")
            }
        }
    }
    
    //==================TESTCASE 3===================//
    func testCheckProjectUrls() {
        var projects = JSONToObjectConverter.SharedInstance.ConvertToProjects(jsonProjects!)

        for project in projects {
            XCTAssertNotNil(project.url, "PROJECT: URL should not be nil")
            XCTAssertNotNil(project.name, "PROJECT: Name of project should not be nil")
            XCTAssertNotNil(project.imageUrl, "PROJECT: Project should have images")
            XCTAssertNotNil(project.unreadItems, "PROJECT: Project should have a unread item count, even zero")
        }
    }

    //==================TESTCASE 4===================//
  /*  func testImageProjectsExist() {
        var projects = JSONToObjectConverter.get.ConvertToProjects(jsonProjects!)
        
        for project in projects {
            var httpUrl = project.imageUrl
            var imageLoaded:Bool
            
            if let data = NSData(contentsOfURL: NSURL(string: httpUrl!)!) {
               imageLoaded = true
            }
            else {
                imageLoaded = false
            }
            
            println(imageLoaded)
        }
    }*/

    //==================TESTCASE 5===================//
    func testNotificationValues() {
        var notifications = JSONToObjectConverter.SharedInstance.ConvertToNotifications(jsonNotifications!)
        
        XCTAssertGreaterThanOrEqual(notifications.count, 1, "The notifications array should be greater then or equal 1")
        
        for notification in notifications {
            XCTAssertNotNil(notification.unread, "NOTIFICATION: Read status not set")
            XCTAssertNotNil(notification.thumbnailUrl, "NOTIFICATION: Thumbnail URL not set")
            XCTAssertNotNil(notification.timestamp, "NOTIFICATION: Time of notification not set")
            XCTAssertNotNil(notification.name, "NOTIFICATION: Title of notification not set")
        }
    }
    
    //==================TESTCASE 6===================//
    func testNotificationTimeStamp() {
        var notifications = JSONToObjectConverter.SharedInstance.ConvertToNotifications(jsonNotifications!)
        let nvc = NotificationViewController()

        for notification in notifications {
         
            if let timestamp = notification.timestamp as String? {
                XCTAssertNotNil(nvc.convertTimestampToDiffCurrentDate(timestamp), "TimeStamp should not be nil")
            }
            else {
                XCTAssertTrue(false, "Notification has no timestamp value")
            }
        }
    }
    
    //==================TESTCASE 7===================//
    func testNotificationHardCoded() {
        var notifications = JSONToObjectConverter.SharedInstance.ConvertToNotifications(jsonNotifications!)
        var index = 0
        for notification in notifications {
            if index == 0 {
            XCTAssertEqual(notification.name!, "iPhone app ontwikkelen", "Name is not correct")
            XCTAssertEqual(notification.oneLiner!, "Flip van Haaren vindt jouw reactie in 'iPhone app ontwikkelen' leuk.", "onliner is not correct")
            XCTAssertEqual(notification.thumbnailUrl!, "https://files.projectcamp.us/files/54fd90e82b99a0480e8b4567/view", "thumbnail URL is not correct")
            XCTAssertEqual(notification.timestamp!, "2015-06-02T18:59:42+02:00", "timestamp is not correct")
            XCTAssertEqual(notification.type!, "comment_like", "type is not correct")
            }
            
            if index == 8 {
                println("LOOK HERE: " + notification.timestamp!)
                XCTAssertEqual(notification.name!, "iPhone app ontwikkelen", "Name is not correct")
                XCTAssertEqual(notification.oneLiner!, "Raymond Jelierse heeft gereageerd op een bericht in 'iPhone app ontwikkelen'.", "onliner is not correct")
                XCTAssertEqual(notification.thumbnailUrl!, "https://files.projectcamp.us/files/54fd90e82b99a0480e8b4567/view", "thumbnail URL is not correct")
                XCTAssertEqual(notification.timestamp!, "2015-06-01T11:58:40+02:00", "timestamp is not correct")
                XCTAssertEqual(notification.type!, "item_comment", "type is not correct")
            }
            
            index++
        }
    }
    
    //==================TESTCASE 8===================//
    func testNotificationHardCodedRegularExpression() {
        var notifications = JSONToObjectConverter.SharedInstance.ConvertToNotifications(jsonNotifications!)
        let regex = NSRegularExpression(pattern: ".*[^A-Za-z0-9\\/.:].*", options: nil, error: nil)!
        
        var trueOrFalse:Bool = false
        
        for notification in notifications {
            
            if regex.firstMatchInString(notification.thumbnailUrl!, options: nil, range: NSMakeRange(0, count(notification.thumbnailUrl!))) == nil {
                //regex correct
                trueOrFalse = true
            }
            else {
                //regex not correct
                trueOrFalse = false
            }
        
            //check if value is true (regex correct)
            XCTAssertEqual(trueOrFalse, true)
            
        }
    }
}

