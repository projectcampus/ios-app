//
//  Authorisation.swift
//  test api oauth
//
//  Created by Jeroen Zonneveld on 14-04-15.
//  Copyright (c) 2015 Zonneveld Cloud. All rights reserved.
//

import UIKit
import Alamofire
import OAuthSwift
import SwiftyJSON

/// Shared instance for singleton authorisation
private var _SharedInstance:Authorisation?

//

/**
    Protocol that is used to refresh the tableview for the overview. Will activate when data from Projectcampus is returned.
*/
protocol AuthorisationDelegate {
    /**
        Start table view data collection
    */
    func startTableViewDataCollection()
}


///    The Authorisation class is used to autorise a Projectcampus user with the Projectcampus app with oAuth 2.0. The authorisation uses a singleton pattern. To call the Authorisation class, you have to call Authorisation.get.

public class Authorisation {
    
    //Local storage Data
    
    ///Local storage for all courses
    var courses = [Course]()
    
    ///Local storage for all projects
    var projects = [Project]()
    
    ///Local storage for all workspaces
    var workspaces = [Workspace]()
    
    ///Local storage for all ProjectFollow
    var projectFollow = [ProjectFollow]()
    
    ///Local storage for all ProjectCoach
    var projectCoach = [ProjectCoach]()
    
    /// local storage for notifications
    private var _notifications = [Notification]()
    
    /// local storage for JSON formats
    var JSONData = JSON?()
    
    /// Total of unread items for courses
    var coursesUnreadItems = Int()
    
    /// Total of unread items for projects
    var projectsUnreadItems = Int()
    
    /// Total of unread items for workspaces
    var workspacesUnreadItems = Int()
    
    /// Total of unread items for projects follow
    var projectFollowUnreadItems = Int()
    
    /// Total of unread items for projects coach
    var projectCoachUnreadItems = Int()
    
    /**
        The get class will return a Authorisation class only once. It is using a singleton pattern
    */
    public class var SharedInstance: Authorisation {
        
       if(!Router.startedSession) {
            _SharedInstance = Authorisation()
            Router.startedSession = true
        }
        
        return _SharedInstance!
    }
    
    
    /**
        This function will reset the router session and will set the oAuthToken to nil.
    */
    public func logout() {
        Router.startedSession = false
        Router.OAuthToken = nil
    }
    
    ///Authorisation delegate for refreshing the table view
    var delegate: AuthorisationDelegate?
        
    /**
        This function will get the config from the Projectcampus config file and put the config in the configuration.
    */
    private init() {
        Alamofire.request(.GET, Config.Links.Settings)
            .responseJSON { (_, _, configInJSON, _) in
                //println(configInJSON)
                
                let json = JSON(configInJSON!)
                //    println(json)
                
                Config.Authorization.AuthorizationUrl = json["oauth"]["authorization_url"].stringValue
                Config.Authorization.ClientId = json["oauth"]["client_id"].stringValue
                Config.Authorization.ClientSecret = json["oauth"]["client_secret"].stringValue
                Config.Authorization.TokenUrl = json["oauth"]["token_url"].stringValue

                self.connect()
        }

    }
    
    
    /**
        Start the connection with Projectcampus API.
    */
    public func connect(){
        let oauthswift = OAuth2Swift(
            consumerKey:    Config.Authorization.ClientId!,
            consumerSecret: Config.Authorization.ClientSecret!,
            authorizeUrl:   Config.Authorization.AuthorizationUrl!,
            accessTokenUrl: Config.Authorization.TokenUrl!,
            responseType:   Config.Authorization.ResponseType
        )
        
        oauthswift.authorizeWithCallbackURL( NSURL(string: Config.Links.CallBack)!, scope: "", state: "PROJECTCAMPUS", success: {
            credential, response in
            
            Router.OAuthToken = credential.oauth_token
            
            println("oauth token set")
            
            if let pushToken = Router.pushToken as String? {
                var tokens:[String:AnyObject] = ["device_token":pushToken]
                let task = Alamofire.request(Router.SendPushNotificationId(tokens))
            }
            
            self.delegate?.startTableViewDataCollection()
            
            }, failure: {(error:NSError!) -> Void in
                println(error.localizedDescription)
                
        })
        
    }
    
    /**
        Empty function. This is overrided in the overviewTableviewController
    */
    public func startTableViewDataCollection(){
        //This is overrided in the overviewTableviewController
    }
    
    /**
        This Router is used to get the information from the ProjectCampus API. You can recieve content from the API by calling the correct case.
    */
    enum Router: URLRequestConvertible {
        
        ///The default url
        static let baseURLString = Config.Links.Base
        
        ///The oAuth token will be placed here
        static var OAuthToken: String?
        
        ///The push notification token will be placed here
        static var pushToken: String?
        
        ///Check if the session is started for creating a oauth connection
        static var startedSession:Bool = false
        
        case ReadCourse() // Recieve all the courses
        case ReadProject() // Recieve all the projects
        case ReadWorkspace() // Recieve all the workspaces
        case ReadProjectFollow() // Recieve all the projects that the person follow
        case ReadProjectCoach()  // Recieve all the projects that the person coach
        case ReadNotifications() // Recieve all the notifications
        case SendPushNotificationId([String: AnyObject]) // Recieve all the notifications
        
        var method: Alamofire.Method {
            switch self {
            case .ReadCourse:
                return .GET
            case .ReadProject:
                return .GET
            case .ReadWorkspace:
                return .GET
            case .ReadProjectFollow:
                return .GET
            case .ReadProjectCoach:
                return .GET
            case .ReadNotifications:
                return .GET
            case .SendPushNotificationId:
                return .POST
            }
        }
        
        var path: String {
            switch self {
            case .ReadCourse():
                return "/users/me/courses"
            case .ReadProject():
                return "/users/me/projects"
            case .ReadWorkspace():
                return "/users/me/subcourses"
            case .ReadProjectFollow():
                return "/users/me/follows"
            case .ReadProjectCoach():
                return "/users/me/coaches"
            case .ReadNotifications():
                return "/notifications"
            case .SendPushNotificationId:
                return "/apns/register"
            }
        }
        //Authorization: Bearer <access-token>
        // MARK: URLRequestConvertible
        
        var URLRequest: NSURLRequest {
            let URL = NSURL(string: Router.baseURLString)!
            let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
            mutableURLRequest.HTTPMethod = method.rawValue
            
            //set the token
            if let token = Router.OAuthToken {
                mutableURLRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
//                println("\(token)")
                println("OauthToken has been set in the Router")
                
            }
            
            
            switch self {
            case .SendPushNotificationId(let parameters):
//                println(mutableURLRequest)
                println("SendPushNotificationID")
                return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
            default:
                return mutableURLRequest
            }
        }
    }
    
    
    var coursesLoaded = false, projectsLoaded = false, workspaceLoaded = false, projectFollowLoaded = false, projectCoachLoaded = false
    
    /**
        This function is automatically called when the user requests the items
    */
    func dataIsCollected(sender: AnyObject) {
        if coursesLoaded && projectsLoaded && workspaceLoaded && projectFollowLoaded && projectCoachLoaded{
            loadDataInTableView(sender)
        }
    }
    
    var notificationLoaded = false
    
    /**
        This function is automatically called when the user requests the notifications
    */
    func notificationsCollected(sender: AnyObject) {
        if notificationLoaded {
            loadDataInTableView(sender)
        }
    }
    
    /**
        Load the data into a table view.
    */
    func loadDataInTableView(anyObject: AnyObject?) {
        if let UITableview = anyObject as? OverviewTableViewController {
            UITableview.updateTableView(courses, projects: projects, workspaces: workspaces, projectsCoach: projectCoach, projectsFollow: projectFollow)
        }
        else if let UITableview = anyObject as? NotificationViewController {
            println("Authorisation: updateTableView")
            UITableview.updateTableView(_notifications)
        }
    }

    /**
    Uses AlamoFire to collect the Courses from the API
    
    :param: AnyObject     The sender. If it's an UITableView than this will be automatically updated
    
    Stores them in the variable "courses"
    */
    func getCourses(sender: AnyObject) {
        var courses = [Course]()
    
        let task = Alamofire.request(Router.ReadCourse()).responseJSON { (req, res, json, error) in
            if(error != nil) {
                NSLog("Error: \(error)")
                println("request \(req)")
                println("response \(res)")
                println("json \(json)")
            }
            else {
                if json != nil {
                    if let jsonData = JSON(rawValue: json!) {
                        courses = JSONToObjectConverter.SharedInstance.ConvertToCourses(jsonData)
                    }
                }
            }
            self.courses = courses
            self.coursesLoaded = true
            self.dataIsCollected(sender)
        }
    }

    /**
    Uses AlamoFire to collect the Projects from the API
    
    :param: AnyObject     The sender. If it's an UITableView than this will be automatically updated
    
    Stores them in the variable "projects"
    */
    func getProjects(sender: AnyObject) {
        var projects = [Project]()
        
        let task = Alamofire.request(Router.ReadProject()).responseJSON { (req, res, json, error) in
            if(error != nil) {
                NSLog("Error: \(error)")
                println("request \(req)")
                println("response \(res)")
                println("json \(json)")
            }
            else {
                if json != nil {
                    if let jsonData = JSON(rawValue: json!) {
                        projects = JSONToObjectConverter.SharedInstance.ConvertToProjects(jsonData)
                    }
                }
            }
            self.projects = projects
            self.projectsLoaded = true
            self.dataIsCollected(sender)
        }
    }
    
    /**
    Uses AlamoFire to collect the Workspaces from the API
    
    :param: AnyObject     The sender. If it's an UITableView than this will be automatically updated
    
    Stores them in the variable "workspaces"
    */
    func getWorkspaces(sender: AnyObject) {
        var workspaces = [Workspace]()
        
        let task = Alamofire.request(Router.ReadWorkspace()).responseJSON { (req, res, json, error) in
            if(error != nil) {
                NSLog("Error: \(error)")
                println("request \(req)")
                println("response \(res)")
                println("json \(json)")
            }
            else {
                if json != nil {
                    if let jsonData = JSON(rawValue: json!) {
                        workspaces = JSONToObjectConverter.SharedInstance.ConvertToWorkspaces(jsonData)
                    }
                }
            }
            self.workspaces = workspaces
            self.workspaceLoaded = true
            self.dataIsCollected(sender)
        }
    }

    /**
    Uses AlamoFire to collect the Notifications from the API
    
    :param: AnyObject     The sender. If it's an UITableView than this will be automatically updated
    
    Stores them in the variable "_notifications"
    */
    func getNotifications(sender: AnyObject) {
        var notifications = [Notification]()

        
        if let notificationController = sender as? NotificationViewController {
            
        
        let task = Alamofire.request(Router.ReadNotifications()).responseJSON { (req, res, json, error) in
            if(error != nil) {
                NSLog("Error: \(error)")
                println("request \(req)")
                println("response \(res)")
                println("json \(json)")
            }
            else {
                if json != nil {
                    if let jsonData = JSON(rawValue: json!) {
                        self.JSONData = jsonData
                        notifications = JSONToObjectConverter.SharedInstance.ConvertToNotifications(jsonData)
                    }
                }
            }
            self._notifications = notifications
            self.notificationLoaded = true
            self.notificationsCollected(sender)
            }
        }
    }
    
    /**
    Uses AlamoFire to collect the projectsFollow from the API
    
    :param: AnyObject     The sender. If it's an UITableView than this will be automatically updated
    
    Stores them in the variable "projectsFollow"
    */
    func getProjectsFollow(sender: AnyObject) {
        println("Look here")
        var projectsFollow = [ProjectFollow]()
        
        let task = Alamofire.request(Router.ReadProjectFollow()).responseJSON { (req, res, json, error) in
            if(error != nil) {
                NSLog("Error: \(error)")
                println("request \(req)")
                println("response \(res)")
                println("json \(json)")
            }
            else {
                if json != nil {
                    if let jsonData = JSON(rawValue: json!) {
                        projectsFollow = JSONToObjectConverter.SharedInstance.ConvertToProjectsFollow(jsonData)
                    }
                }
            }
            self.projectFollow = projectsFollow
            self.projectFollowLoaded = true
            self.dataIsCollected(sender)
        }
   }

    /**
    Uses AlamoFire to collect the projectsCoach from the API
    
    :param: AnyObject     The sender. If it's an UITableView than this will be automatically updated
    
    Stores them in the variable "projectsCoach"
    */
    func getProjectsCoach(sender: AnyObject) {
        var projectsCoach = [ProjectCoach]()
        
        let task = Alamofire.request(Router.ReadProjectCoach()).responseJSON { (req, res, json, error) in
            if(error != nil) {
                NSLog("Error: \(error)")
                println("request \(req)")
                println("response \(res)")
                println("json \(json)")
            }
            else {
                if json != nil {
                    if let jsonData = JSON(rawValue: json!) {
                        projectsCoach = JSONToObjectConverter.SharedInstance.ConvertToProjectsCoach(jsonData)
                    }
                }
            }
            self.projectCoach = projectsCoach
            self.projectCoachLoaded = true
            self.dataIsCollected(sender)
        }
    }
    
    /**
        Returns the oAuth token 
    */
    public func getToken() -> String {
        var token = Router.OAuthToken
        
        if(token == nil) {
            token = ""
        }
        
        return token!
    }
    
    /**
        Set the oAuth token for external functions. This function is not used for the Projectcampus oAuth authorisation.
    */
    public func setToken(token:String) {
        Router.OAuthToken = token
    }
    
    /**
    Set the push notification token in the router so you can send it with the API.
    */
    public func setPushNotificationToken(token:String) {
        Router.pushToken = token
    }
}