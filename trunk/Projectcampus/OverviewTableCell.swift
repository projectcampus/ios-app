//
//  OverviewTableCell.swift
//  Projectcampus
//
//  Created by Johan van der Meulen on 21/04/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import Foundation
import UIKit

/**
    Custom cell of the overview table. Contains image, title and number of courses inside.
*/
class OverviewTableCell: UITableViewCell {
    
    /// Unread messages
    @IBOutlet weak var unreadMessagesLabelOutlet: UILabel!
    
    ///label with the title
    @IBOutlet weak var labelTitle: UILabel!
    
    ///Image of the element
    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    ///Value of total child-elements
    @IBOutlet weak var numberOfItemsLabelOutlet: UILabel!
    
    ///Check if element needs opacity
    @IBOutlet weak var alphaView: UIView!
    
    ///Stores the bool if the cell is disabled due to no items 
    var disabled = false
    
//    func ResetSelectionStyle() {
//        self.selectionStyle = UITableViewCellSelectionStyle.None
//    }
}

