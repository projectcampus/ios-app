//
//  settingsWebViewController.swift
//  Projectcampus
//
//  Created by Johan van der Meulen on 19/05/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit

/// The settingsclass for the webview
class settingsWebViewController : UIViewController, UIWebViewDelegate {

    /// The outlet for the webview
    @IBOutlet weak var webView: UIWebView!
    
    /// The title for the view
    var nameOfPage:String?
    
    /// The Url to be loaded in the webview
    var urlForWebview:String?
    
    /// The logout boolean
    var logout:Bool = false
    
    /// Initialises the settings view
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(logout) {
            webView.hidden = true
            
            var AuthorizationObject = Authorisation.SharedInstance
            AuthorizationObject.logout()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateInitialViewController() as! LoginViewController
            self.presentViewController(vc, animated: true, completion: nil)
        }
        else {
        var connection = Authorisation.SharedInstance
        var token = connection.getToken()
        
        if urlForWebview == nil {
            urlForWebview = Config.Links.Url
        }
        var more = "#access_token=\(token)"
        var completeURL = Config.Links.Url + urlForWebview! + more
        println(completeURL)
        
        webView.loadRequest(NSURLRequest(URL: NSURL(string: completeURL)!))
        }
        
        self.title = nameOfPage
    }
}