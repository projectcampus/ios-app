//
//  Notification.swift
//  Projectcampus
//
//  Created by Jeroen Zonneveld on 11-05-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit

/// The notification object
public class Notification {
    private var _id: String
    private var _fromUser : String?
    private var _timestamp : String?
    private var _type : String?
    private var _oneLiner : String?
    private var _unread : Bool?
    private var _location : String?
    private var _thumbnailURL : String?
    private var _name : String?
    
    /**
    Initialises an Notification Object
    
    :param: String     The id for this object.
    */
    init(id:String) {
        self._id = id
    }
    
    /**
    Returns the notification id
    
    :returns: String     The id for this object.
    */
    var id: String {
        get {
            return self._id
        }
    }
    
    /**
    Set/Returns the user who invoked this notification.
    
    :param: String?    Sets the name of the user.
    
    :returns: String?   Returns the name of the user.
    */
    var fromUser: String? {
        get {
            return self._fromUser
        }
        set(fromUser) {
            self._fromUser = fromUser
        }
    }
    
    /**
    Set/Returns the timestamp of this notification.
    
    :param: String?    Sets the timestamp of this notification.
    
    :returns: String?   The timestamp of this notification.
    */
    var timestamp: String? {
        get {
            return self._timestamp
        }
        set(timestamp) {
            self._timestamp = timestamp
        }
    }
    
    /**
    Set/Returns the type of this notification.
    
    :param: String?    Sets the type of this notification.
    
    :returns: String?   The type for this notification.
    */
    var type: String? {
        get {
            return self._type
        }
        set(type) {
            self._type = type
        }
    }
    
    /**
    Set/Returns the oneliner of this object.
    
    :param: String?    Sets the oneliner of this object.
    
    :returns: String?   The oneliner of this object.
    */
    var oneLiner: String? {
        get {
            return self._oneLiner
        }
        set(oneLiner) {
            self._oneLiner = oneLiner
        }
    }

    /**
    Set/Returns if the notification is unread.
    
    :param: Bool?    Sets the unread bool of this notification.
    
    :returns: Bool?   The unread bool status of this notification.
    */
    var unread: Bool? {
        get {
            return self._unread
        }
        set(unread) {
            self._unread = unread
        }
    }
    
    /**
    Set/Returns the location from where this notification has been invoked.
    
    :param: String?    Sets the location of this notification.
    
    :returns: String?   The location of this notification.
    */
    var location: String? {
        get {
            return self._location
        }
        set (location) {
            self._location = location
        }
    }
    
    /**
    Set/Returns the thumbnailUrl of this notification.
    
    :param: String?    Sets the thumbnailUrl of this notification.
    
    :returns: String?   The thumbnailUrl of this notification.
    */
    var thumbnailUrl: String? {
        get {
            return self._thumbnailURL
        }
        set(thumbnailUrl) {
            self._thumbnailURL = thumbnailUrl
        }
    }
    
    /**
    Set/Returns the name of this notification.
    
    :param: String?    Sets the name of this notification.
    
    :returns: String?   The name of this notification.
    */
    var name: String? {
        get {
            return self._name
        }
        set(name) {
            self._name = name
        }
    }
}