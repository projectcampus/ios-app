//
//  NavigationViewController.swift
//  Projectcampus
//
//  Created by Jeroen Zonneveld on 12-05-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit

///The navigation controller for projectCampus
class NavigationViewController : UINavigationController {
    
    /**
        This function will start the navigation view controller for the overview tab
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set background to blue
        UINavigationBar.appearance().barTintColor = Config.Color.Header.Background
        
        //set back button collor to white
        UINavigationBar.appearance().tintColor = Config.Color.Header.Text
        
        // set title color to white
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: Config.Color.Header.Text]
        UINavigationBar.appearance().titleTextAttributes = titleDict as [NSObject : AnyObject]
        
        //var tabArray = self.tabBarController?.tabBar.items as NSArray!
       // var tabItem = tabArray.objectAtIndex(1) as! UITabBarItem
       // tabItem.badgeValue = "34"
    }
}