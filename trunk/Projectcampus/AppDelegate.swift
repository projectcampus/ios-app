//
//  AppDelegate.swift
//  Projectcampus
//
//  Created by Jeroen Zonneveld on 14-04-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit
import OAuthSwift
import Alamofire

@UIApplicationMain

/// The AppDelegate protocol defines methods that are called by the singleton
class AppDelegate: UIResponder, UIApplicationDelegate {

    /// Window outlet that is used to go to another screen
    var window: UIWindow?

    /**
        Recieve oAuth token and safe it in the Router. After that, it will show the overview page with courses, projects and more...
    */
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        if (url.host == "oauth-callback") {
            if ( url.path!.hasPrefix("/projectcampus" )){
                OAuth2Swift.handleOpenURL(url)
            
                var storyboard: UIStoryboard = UIStoryboard(name: "Overview", bundle: nil)
            
                self.window?.rootViewController = storyboard.instantiateInitialViewController() as! UITabBarController
                self.window?.makeKeyAndVisible()
            }
        }

        return true
    }
    
    /**
        Set the browser agent to Projectcampus (from config). Also change the status bar to white.
    */
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        
        ///set user agent for webbrowser to Projectcampus for mobile view
        NSUserDefaults.standardUserDefaults().registerDefaults(["UserAgent" : Config.UserAgent])
        
        //carrier text white        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
        return true
    }
    
    /**
        Function that will be activated when a user agreed to recieve push notifications
    */
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let authorisation = Authorisation.SharedInstance
        authorisation.setPushNotificationToken("\(deviceToken)")
    }
    
    /**
    Function that will be activated when a user disagreed on recieving push notifications
    */
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        println(error)
        
        
    }
    

    
}

