//
//  CourseWebViewController.swift
//  Projectcampus
//
//  Created by Jeroen Zonneveld on 21-04-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit

/// The controller for the webview
class ItemWebViewController : UIViewController, UIWebViewDelegate {
    
    /// The outlet of the webview
    @IBOutlet weak var webView: UIWebView!
    
    /// The url for the webview to load
    var urlForWebview:String?
    
    /// Initialises the webview
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var connection = Authorisation.SharedInstance
        var token = connection.getToken()
        
        if urlForWebview == nil {
            urlForWebview = Config.Links.Url
        }
        var more = "/items#access_token=\(token)"
        var completeURL = Config.Links.Url + urlForWebview! + more
        println(completeURL)
        
        webView.loadRequest(NSURLRequest(URL: NSURL(string: completeURL)!))
        
         tabBarController?.hidesBottomBarWhenPushed = true;
    }
}