//
//  Course.swift
//  test api oauth
//
//  Created by Jeroen Zonneveld on 14-04-15.
//  Copyright (c) 2015 Zonneveld Cloud. All rights reserved.
//

/// The UserItem superclass
public class UserItem {
    private let _id:String
    private var _name:String?
    private var _url:String?
    private var _imageUrl:String?
    private var _unreadItems:Int?
    

    /**
    Initialises an UserItem
    
    :param: String     The id for this object.
    */
    init(id:String) {
        self._id = id
    }
    
    /**
    Returns the Object id.
    
    :returns: String The id of the object.
    */
    var id: String {
        get {
            return self._id
        }
    }
    
    /**
    Set/Returns the name of this object.
    
    :param: String?    Sets the name of this object.
    
    :returns: String?   The name of this object.
    */
    var name: String? {
        get {
            return self._name
        }
        set(name) {
            self._name = name
        }
    }
    
    /**
    Set/Returns the url of this object.
    
    :param: String?    Sets the url of this object.
    
    :returns: String?   The url of this object.
    */
    var url: String? {
        get {
            return self._url
        }
        set(url) {
            self._url = url
        }
    }
    
    /**
    Set/Returns the imageUrl of this object.
    
    :param: String?    Sets the imageUrl of this object.
    
    :returns: String?   The imageUrl of this object.
    */
    var imageUrl: String? {
        get {
            return self._imageUrl
        }
        set(imageUrl) {
            self._imageUrl = imageUrl
        }
    }
    
    /**
    Set/Returns the unread items of this object.
    
    :param: Int?    Sets the amount of unread items for this object.
    
    :returns: Int?   The amount of unread items for this object.
    */
    var unreadItems: Int? {
        get {
            if let unreadItems = self._unreadItems as Int! {
                return self._unreadItems
            }
            else {
                return 0
            }
        }
        set(unreadItems) {
            self._unreadItems = unreadItems
        }
    }
    
}