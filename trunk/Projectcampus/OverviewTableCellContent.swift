//
//  OverviewTableCellContent.swift
//  Projectcampus
//
//  Created by Johan van der Meulen on 29/04/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

/**
Type of item

- Course
- Project
- Workspace
- ProjectFollow
- ProjectCoach
- Default
*/
enum type {
    case Course, Project, Workspace, ProjectFollow, ProjectCoach, Default
}

/**
    The content of the overview table cell
*/
class OverviewTableCellContent {
    var _title = String()
    var _numberOfUnreadMessagesTotal = Int()
    var _type : type
    var _numberOfItemsTotal = Int()
    
    init (title: String, numberOfUnreadMessages: Int, Buttontype: type, numberOfItemsTotal: Int) {
        _title = title
        _numberOfUnreadMessagesTotal = numberOfUnreadMessages
        _type = Buttontype
        _numberOfItemsTotal = numberOfItemsTotal
    }
}