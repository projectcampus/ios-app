//
//  OverviewTableViewController.swift
//  Projectcampus
//
//  Created by Johan van der Meulen on 21/04/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import Foundation
import UIKit

/// The tableview controller of the overview
class OverviewTableViewController: UITableViewController, AuthorisationDelegate {
    
    /// The tableview for this controller
    @IBOutlet var tableviewOutlet: UITableView!
    
    /// The cache variable that holds all the content for this application
    var menuItems = [OverviewTableCellContent]()
    
    /// The bool that holds the tokenRecieved value
    var tokenRecieved = false
    
    /// Initialises the OverViewTableView controller
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor(red:0.957, green:0.984, blue:0.992, alpha:1) /*#f4fbfd*/
        tableView.tableFooterView = UIView(frame:CGRectZero)
        
        //PlaceHolder buttons
        if !tokenRecieved {
            menuItems.append(OverviewTableCellContent(title: "Courses", numberOfUnreadMessages: 0, Buttontype: type.Course, numberOfItemsTotal: 0))
            menuItems.append(OverviewTableCellContent(title: "Projects", numberOfUnreadMessages: 0, Buttontype: type.Project, numberOfItemsTotal: 0))
            menuItems.append(OverviewTableCellContent(title: "Projects Follow", numberOfUnreadMessages: 0, Buttontype: type.ProjectFollow, numberOfItemsTotal: 0))
            menuItems.append(OverviewTableCellContent(title: "Projects Coach", numberOfUnreadMessages: 0, Buttontype: type.ProjectCoach, numberOfItemsTotal: 0))
             menuItems.append(OverviewTableCellContent(title: "Workspaces", numberOfUnreadMessages: 0, Buttontype: type.Workspace, numberOfItemsTotal: 0))
        }
        
        Authorisation.SharedInstance.delegate = self
        
        
        
        UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: UIUserNotificationType.Sound | UIUserNotificationType.Alert | UIUserNotificationType.Badge, categories: nil))
        UIApplication.sharedApplication().registerForRemoteNotifications()
        
        tableviewOutlet.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        tableviewOutlet.separatorInset = UIEdgeInsetsZero
        
        tableviewOutlet.scrollEnabled = false
}
    
    ///Initialises tableviewdata when the oauthToken has been set
    func startTableViewDataCollection() {
        println("delegate invoked")
        tokenRecieved = true
        
        Authorisation.SharedInstance.getCourses(self)
        Authorisation.SharedInstance.getProjects(self)
        Authorisation.SharedInstance.getWorkspaces(self)
        Authorisation.SharedInstance.getProjectsFollow(self)
        Authorisation.SharedInstance.getProjectsCoach(self)
        println("delegate Done")
    }
    
    ///updates the tableview when the data has been loaded
    func updateTableView(courses: [Course], projects: [Project], workspaces: [Workspace], projectsCoach: [ProjectCoach], projectsFollow: [ProjectFollow]) {
        
        loadUnreadMessages(menuItems)
        loadNumberOfitemsTotal(menuItems, courses: courses, projects: projects, workspaces: workspaces, projectsCoach: projectsCoach, projectsFollow: projectsFollow)
        tableviewOutlet.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Sets the amount of cells to create in the tableview
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return menuItems.count
        
    }
    
    /// Initialises the cells and the tableview
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("customCell") as! OverviewTableCell

        cell.imageViewOutlet.hidden = false
        cell.alpha = 1
        if !tokenRecieved {
            cell.labelTitle.text = menuItems[indexPath.item]._title
            cell.numberOfItemsLabelOutlet.hidden = true
        }
        else {
            cell.labelTitle.text = menuItems[indexPath.item]._title
            
            //Setting the numberOfItems label
            if menuItems[indexPath.item]._numberOfItemsTotal > 0 {
                let totalItems = menuItems[indexPath.item]._numberOfItemsTotal
                cell.numberOfItemsLabelOutlet.hidden = false
                cell.numberOfItemsLabelOutlet.text = "\(totalItems)"
                cell.userInteractionEnabled = true
                cell.alphaView.hidden = true
                cell.disabled = false
            }
            else {
                cell.numberOfItemsLabelOutlet.hidden = true
//                cell.userInteractionEnabled = false
                cell.disabled = true
            }
            
            cell.imageViewOutlet.image = setImage(menuItems[indexPath.item]._title)
        }
        return cell
    }
    
    /// Segues to the specifiedOverviewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if tokenRecieved {
            if let dvc = segue.destinationViewController as? SpecifiedOverviewTableViewController {
                if let button = sender as? OverviewTableCell{
                    if button.disabled == false {
                        switch button.labelTitle.text! {
                        case "Courses":
                            dvc.menuItemType = type.Course
                            break
                        case "Projects":
                            dvc.menuItemType = type.Project
                            break
                        case "Workspaces":
                            dvc.menuItemType = type.Workspace
                            break
                        case "Projects Follow":
                            dvc.menuItemType = type.ProjectFollow
                            break
                        case "Projects Coach":
                            dvc.menuItemType = type.ProjectCoach
                            break
                        default:
                            println("ERROR NO CASE")
                            break
                        }
                        dvc.testTitle = button.labelTitle.text!
                    }
                }
            }
        }
    }
    
    /**
    Set the image of the tableview cells
    
    :param: String title of the image
    
    :return: UIImage the corresponding image
    */
    func setImage (title: String) -> UIImage {
        var image = UIImage()
        switch title {
        case "Courses":
            image = UIImage(named: "Courses")!
            break
        case "Projects":
            image = UIImage(named: "projecten")!
            break
        case "Workspaces":
            image = UIImage(named: "werkplekken")!
            break
        case "Projects Follow":
            image = UIImage(named: "projectenvolg")!
            break
        case "Projects Coach":
            image = UIImage(named: "projectencoach")!
            break
        default:
            println("ERROR NO CASE")
            break
        }
        print("IMAGESET: " + title + " /")
        return image
    }

    /**
    Loads al the messages that have not been red
    
    :param: [OverviewTableCellContent] content to check
    */
    func loadUnreadMessages(items: [OverviewTableCellContent]){
        
        for item in items {
         
            switch item._type {
            case type.Course:
                item._numberOfUnreadMessagesTotal = Authorisation.SharedInstance.coursesUnreadItems
                break
            case type.Project:
                item._numberOfUnreadMessagesTotal = Authorisation.SharedInstance.projectsUnreadItems
                break
            case type.Workspace:
                item._numberOfUnreadMessagesTotal = Authorisation.SharedInstance.workspacesUnreadItems
                break
            case type.ProjectFollow:
                item._numberOfUnreadMessagesTotal = Authorisation.SharedInstance.projectFollowUnreadItems
                break
            case type.ProjectCoach:
                item._numberOfUnreadMessagesTotal = Authorisation.SharedInstance.projectCoachUnreadItems
                break
            default:
                break
            }
        }
    }
    
    /**
    Loads the number of items per category
    
    :param: [OverviewTableCellContent]
    :param: [Course]
    :param: [Project]
    :param: [Workspace]
    :param: [ProjectCoach]
    :param: [ProjectFollow])
    */
    func loadNumberOfitemsTotal(items: [OverviewTableCellContent], courses: [Course], projects: [Project], workspaces: [Workspace], projectsCoach: [ProjectCoach], projectsFollow: [ProjectFollow]) {
        for item in items {
            
            switch item._type {
            case type.Course:
                item._numberOfItemsTotal = courses.count
                break
            case type.Project:
                item._numberOfItemsTotal = projects.count
                break
            case type.Workspace:
                item._numberOfItemsTotal = workspaces.count
                break
            case type.ProjectFollow:
                item._numberOfItemsTotal = projectsFollow.count
                break
            case type.ProjectCoach:
                item._numberOfItemsTotal = projectsCoach.count
                break
            default:
                break
            }
        }
    }
    
    ///Sets the seperator line between the cells
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
    }
    
    ///Sets the highlight color of the cell
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell:OverviewTableCell = tableView.cellForRowAtIndexPath(indexPath) as! OverviewTableCell
        
        if selectedCell.disabled {
            let alertController = UIAlertController(title: "Whoops", message:
                "You don't have any " + "\(selectedCell.labelTitle.text!)" + " ", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else {
          selectedCell.backgroundColor = Config.Color.TableView.TableBlueOnClick.colorWithAlphaComponent(0.1)
        }
    }
    
    ///Resets the color of the cell
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell:OverviewTableCell = tableView.cellForRowAtIndexPath(indexPath) as! OverviewTableCell
        selectedCell.backgroundColor = UIColor.whiteColor()
    }
}

