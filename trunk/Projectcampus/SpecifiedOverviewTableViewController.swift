//
//  SpecifiedOverviewTableViewController.swift
//  Projectcampus
//
//  Created by Johan van der Meulen on 21/04/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import Foundation
import UIKit

/// The controller of the specified tableview
class SpecifiedOverviewTableViewController: UITableViewController {
    
    @IBOutlet weak var UINavItemOutlet: UINavigationItem!
    var imageCache = [String:UIImage]()

    var testTitle = String()
    var menuItemType = type.Default
    var specifiedMenuItems = Array<UserItem>()
    @IBOutlet var tableViewOutlet: UITableView!
    
    override func viewDidLoad() {
        UINavItemOutlet.title = testTitle
        
        switch menuItemType {
        case type.Course:
            specifiedMenuItems = Authorisation.SharedInstance.courses
            break
        case type.Project:
            specifiedMenuItems = Authorisation.SharedInstance.projects
            break
        case type.Workspace:
            specifiedMenuItems = Authorisation.SharedInstance.workspaces
            break
        case type.ProjectFollow:
            specifiedMenuItems = Authorisation.SharedInstance.projectFollow
            break
        case type.ProjectCoach:
            specifiedMenuItems = Authorisation.SharedInstance.projectCoach
            break
        default:
            break
        }
        
        tableViewOutlet.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        tableViewOutlet.separatorInset = UIEdgeInsetsZero
        
        tableView.backgroundColor = UIColor(red:0.957, green:0.984, blue:0.992, alpha:1) /*#f4fbfd*/
        tableView.tableFooterView = UIView(frame:CGRectZero)
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return specifiedMenuItems.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("customSpecifiedCell") as! SpecifiedTableCell
        cell.id = indexPath.item
        
        var menuItem = specifiedMenuItems[indexPath.item]
//      println("\(menuItem)" + "Look here")
        
        //Title
        if let cellTitle = menuItem.name as String? {
            cell.labelTitle.text = cellTitle
        }
            
        //Unread messages //Deprecated
        let numberOfUnreadMessages = menuItem.unreadItems!
        
        //Icon image
        cell.imageViewOutlet.image = UIImage(named: "AppIcon")
        
        if let img = imageCache[specifiedMenuItems[indexPath.item].imageUrl!] {
            cell.imageViewOutlet.contentMode = UIViewContentMode.ScaleAspectFit
            cell.imageViewOutlet.image = img
        }
        else {
            // We should perform this in a background thread
            let imgURL = NSURL(string: specifiedMenuItems[indexPath.item].imageUrl!)
            let request: NSURLRequest = NSURLRequest(URL: imgURL!)
            let mainQueue = NSOperationQueue.mainQueue()
            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    let image = UIImage(data: data)
                    self.imageCache[self.specifiedMenuItems[indexPath.item].imageUrl!] = image
                    // Update the cell
                    dispatch_async(dispatch_get_main_queue(), {
                        if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? SpecifiedTableCell {
                            cellToUpdate.imageViewOutlet.contentMode = UIViewContentMode.ScaleAspectFit
                            cellToUpdate.imageViewOutlet.image = image
                        }
                    })
                }
                else {
                    println("Error: \(error.localizedDescription)")
                }
            })
        }

        
        return cell
    }
    
    let blogSegueIdentifier = "showWebViewData"
    
    //Pressing a tableview cell
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == blogSegueIdentifier {
            if let destination = segue.destinationViewController as? ItemWebViewController {
                if let buttonPressed = sender as? SpecifiedTableCell {
                    destination.title = specifiedMenuItems[buttonPressed.id].name!
                    destination.urlForWebview = specifiedMenuItems[buttonPressed.id].url!
                }
            }
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
    }
    
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! SpecifiedTableCell
        selectedCell.backgroundColor = Config.Color.TableView.TableBlueOnClick.colorWithAlphaComponent(0.1)
    }
    
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! SpecifiedTableCell
        selectedCell.backgroundColor = UIColor.whiteColor()
    }
    
}


