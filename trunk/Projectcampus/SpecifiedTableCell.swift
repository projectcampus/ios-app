//
//  SpecifiedTableCell.swift
//  Projectcampus
//
//  Created by Johan van der Meulen on 29/04/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import Foundation
import UIKit

/// The logic for the specified table cell
class SpecifiedTableCell: UITableViewCell {
    
    /// The label to display the title of this item
    @IBOutlet weak var labelTitle: UILabel!
    
    /// The UIImageView to display the icon for this item
    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    /// The id for this item
    var id = Int()
    
    /// The corresponding url for this project to load in the webView
    var url:String?
    
}