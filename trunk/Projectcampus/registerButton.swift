//
//  registerButton.swift
//  Projectcampus
//
//  Created by danick sikkema on 12-05-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import Foundation
import UIKit

 class registerButton: UIButton {
    required init(coder aDecoder: (NSCoder!)) {
        
        super.init(coder: aDecoder);
        self.layer.cornerRadius = 5.0;
        self.layer.borderColor = UIColor(red:0.153, green:0.671, blue:0.831, alpha:1).CGColor
        self.layer.borderWidth = 1.5;
        self.backgroundColor = UIColor.whiteColor();
        self.tintColor = UIColor (red:0.153, green:0.671, blue:0.831, alpha:1);
    
    }

}
