//
//  notificationViewController.swift
//  Projectcampus
//
//  Created by Jeroen Zonneveld on 11-05-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit

/// The controller for the notification view
class NotificationViewController : UITableViewController {
    
    @IBOutlet var notificationTableViewOutlet: UITableView!
    var imageCache = [String:UIImage]()
    
    var numberOfNotifications = Int()
    var notifications = [Notification]()
    
    /// Initialises the notification controller 
    override func viewDidLoad() {
        var connection = Authorisation.SharedInstance
        connection.getNotifications(self)
        
        notificationTableViewOutlet.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        notificationTableViewOutlet.separatorInset = UIEdgeInsetsZero
        
        tableView.backgroundColor = UIColor(red:0.957, green:0.984, blue:0.992, alpha:1) /*#f4fbfd*/
        tableView.tableFooterView = UIView(frame:CGRectZero)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
    Updates the tableview with the new data
    
    :param: [Notification]
    */
    func updateTableView(Notifications: [Notification]) {
        
        numberOfNotifications = Notifications.count
        notifications = Notifications
        self.notificationTableViewOutlet.reloadData()
        println("UPDATE TABLEVIEW")
    }
    
    ///Sets the number of notification to load
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        println("NumberofNotifications: " + "\(numberOfNotifications)")
        return numberOfNotifications
    }
    
    ///Sets the notification cells and the notification tableview
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        println("TableViewCell Created: " + "\(indexPath.item)")
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as! NotificationTableCell
        cell.id = indexPath.item
        
        if let notificationUnread = notifications[indexPath.item].unread as Bool? {
            if notificationUnread == true {
                cell.backgroundColor = Config.Color.Notification.Unread
                cell.oneLinerTextOutlet.backgroundColor = Config.Color.Notification.Unread
            }
            else {
                cell.backgroundColor = Config.Color.Notification.Read
                cell.oneLinerTextOutlet.backgroundColor = Config.Color.Notification.Read
            }
        }
        
        if let notificationOneLiner = notifications[indexPath.item].oneLiner as String? {
            cell.oneLinerTextOutlet.text = notificationOneLiner
        }
        
        if let timestamp = notifications[indexPath.item].timestamp as String? {
            let components = convertTimestampToDiffCurrentDate(timestamp)
            
            if components.day > 0 {
                cell.timeStampOutlet.text = "\(components.day)" + " days ago"
            }
            else if components.hour > 0 {
                cell.timeStampOutlet.text = "\(components.hour)" + " hours ago"
            }
            else if components.minute > 0 {
                cell.timeStampOutlet.text = "\(components.minute)" + " minutes ago"
            }
            else {
                cell.timeStampOutlet.text = "\(components.second)" + " seconds ago"
            }
            
        }
        else {
            println("timestamp not set")
        }

        
        cell.IconOutlet.image = UIImage(named: "AppIcon")
        
        if let img = imageCache[notifications[indexPath.item].thumbnailUrl!] {
            cell.IconOutlet.contentMode = UIViewContentMode.ScaleAspectFit
            cell.IconOutlet.image = img
        }
        else {
            // We should perform this in a background thread
            let imgURL = NSURL(string: notifications[indexPath.item].thumbnailUrl!)
            let request: NSURLRequest = NSURLRequest(URL: imgURL!)
            let mainQueue = NSOperationQueue.mainQueue()
            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    let image = UIImage(data: data)
                    self.imageCache[self.notifications[indexPath.item].thumbnailUrl!] = image
                    // Update the cell
                    dispatch_async(dispatch_get_main_queue(), {
                        if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? NotificationTableCell {
                            cellToUpdate.IconOutlet.contentMode = UIViewContentMode.ScaleAspectFit
                            cellToUpdate.IconOutlet.image = image
                        }
                    })
                }
                else {
                    println("Error: \(error.localizedDescription)")
                }
            })
        }
    
        cell.oneLinerTextOutlet.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    /**
    converts the data to a usable timestamp
    
    :param: String The timestamp
    
    :returns: NSDateComponents An component object to use
    */
    func convertTimestampToDiffCurrentDate(timeStamp: String) -> NSDateComponents {
        //Converting current timestamp to NSDate
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let timestampDate = formatter.dateFromString(timeStamp)!
        
        //Calculating elapsed time seconds
        let elapsedTime = NSDate().timeIntervalSinceDate(timestampDate)
        
        //Calculating elapsed time in components
        let components = NSCalendar.currentCalendar().components(.CalendarUnitSecond |
            .CalendarUnitMinute | .CalendarUnitHour | .CalendarUnitDay |
            .CalendarUnitMonth | .CalendarUnitYear, fromDate: timestampDate,
            toDate: NSDate(), options: nil)
        
        return components
    }
    
    /// Segues to the itemWebView
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "notificationWebview" {
            if let destination = segue.destinationViewController as? ItemWebViewController {
                if let buttonPressed = sender as? NotificationTableCell {
                    if notifications[buttonPressed.id].unread! == true {
                        notifications[buttonPressed.id].unread! = false
                        tableView.reloadData()
                    }
                    destination.title = notifications[buttonPressed.id].name!
                    destination.urlForWebview = notifications[buttonPressed.id].location
                }
            }
        }
    }
    
    ///Sets the seperator line between the cells
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
    }
    
    ///Sets the highlight color of the cell
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! NotificationTableCell
        selectedCell.backgroundColor = Config.Color.TableView.TableBlueOnClick.colorWithAlphaComponent(0.1)
    }
    
    ///Resets the color of the cell
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! NotificationTableCell
        selectedCell.backgroundColor = UIColor.whiteColor()
    }
    
}