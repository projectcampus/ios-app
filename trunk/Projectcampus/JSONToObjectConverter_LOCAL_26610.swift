//
//  JSONToObjectConverter.swift
//  Projectcampus
//
//  Created by Johan van der Meulen on 01/06/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

private var _sharedInstance = JSONToObjectConverter()

///    JSONToObjectConverter converts a JSON to an object type of choice
public class JSONToObjectConverter {
    
    /**
    Get the JSONToObjectConverter singleton.
    */
    public class var get : JSONToObjectConverter {
        return _sharedInstance
    }
    
    /**
    Converts an JSON object to a Course.
    
    :param: JSON     The Json to convert.
    
    :returns: An array of Course objects.
    */
    func ConvertToCourses(jsonData: JSON) -> [Course]{
        var courses = [Course]()
        if jsonData != nil {
            for course in jsonData.arrayValue {
                
                if let courseId = course["id"].string {
                    let courseObject = Course(id: courseId)
                    
                    if let courseName = course["name"].string {
                        courseObject.name = courseName
                    }
                    
                    if let courseURL = course["url"].string {
                        courseObject.url = courseURL
                    }
                    
                    if let courseImage = course["organization"]["thumbnail_url"].string {
                        courseObject.imageUrl = courseImage
                    }
                    else {
                        courseObject.imageUrl = "https://api-test.projectcamp.us/images/thumb-course-150.png"
                    }
                    
                    if let courseUnread = course["organization"]["unread_items"].int {
                        courseObject.unreadItems = courseUnread
                       // self.coursesUnreadItems += courseUnread
                    }
                    courses.append(courseObject)
                }
            }
        }
        return courses
    }
    
    /**
    Converts an JSON object to a Project.
    
    :param: JSON     The Json to convert.
    
    :returns: An array of Project objects.
    */
    func ConvertToProjects(jsonData: JSON) -> [Project]{
        var projects = [Project]()
        if jsonData != nil {
            for project in jsonData.arrayValue {
                
                if let projectId = project["id"].string {
                    println(projectId)
                    let projectObject = Project(id: projectId)
                    
                    if let projectName = project["name"].string {
                        projectObject.name = projectName
                    }
                    
                    if let projectURL = project["url"].string {
                        projectObject.url = projectURL
                    }
                    
                    if let projectImage = project["thumbnail_url"].string {
                        projectObject.imageUrl = projectImage
                    }
                    
                    if let projectUnread = project["unread_items"].int {
                        projectObject.unreadItems = projectUnread
                      //  self.projectsUnreadItems += projectUnread
                    }
                    
                    projects.append(projectObject)
                }
            }
        }
        return projects
    }
    
    /**
    Converts an JSON object to a ProjectFollow.
    
    :param: JSON     The Json to convert.
    
    :returns: An array of ProjectFollow objects.
    */
    public func ConvertToProjectsFollow(jsonData: JSON) -> [ProjectFollow]{
        var projectsFollow = [ProjectFollow]()
        if jsonData != nil {
             for projectFollow in jsonData.arrayValue {
                if let projectFollowId = projectFollow["id"].string {
                    println(projectFollowId)
                    let projectFollowObject = ProjectFollow(id: projectFollowId)
                    
                    if let projectFollowName = projectFollow["name"].string {
                        projectFollowObject.name = projectFollowName
                    }
                    
                    if let projectFollowURL = projectFollow["url"].string {
                        projectFollowObject.url = projectFollowURL
                    }
                    
                    if let projectFollowImage = projectFollow["thumbnail_url"].string {
                        projectFollowObject.imageUrl = projectFollowImage
                    }
                    
                    if let projectFollowUnread = projectFollow["unread_items"].int {
                        projectFollowObject.unreadItems = projectFollowUnread
                     //   self.projectFollowUnreadItems += projectFollowUnread
                    }
                    
                    projectsFollow.append(projectFollowObject)
                }
            }
        }
        
        return projectsFollow
    }
    
    /**
    Converts an JSON object to a ProjectCoach.
    
    :param: JSON     The Json to convert.
    
    :returns: An array of ProjectCoach objects.
    */
    public func ConvertToProjectsCoach(jsonData: JSON) -> [ProjectCoach]{
        var projectsCoach = [ProjectCoach]()
        if jsonData != nil {
            for projectCoach in jsonData.arrayValue {
                if let projectCoachId = projectCoach["id"].string {
                    println(projectCoachId)
                    let projectCoachObject = ProjectCoach(id: projectCoachId)
                    
                    if let projectCoachName = projectCoach["name"].string {
                        projectCoachObject.name = projectCoachName
                    }
                    
                    if let projectCoachURL = projectCoach["url"].string {
                        projectCoachObject.url = projectCoachURL
                    }
                    
                    if let projectCoachImage = projectCoach["thumbnail_url"].string {
                        projectCoachObject.imageUrl = projectCoachImage
                    }
                    
                    if let projectCoachUnread = projectCoach["unread_items"].int {
                        projectCoachObject.unreadItems = projectCoachUnread
                      //  self.projectCoachUnreadItems += projectCoachUnread
                    }
                    
                    projectsCoach.append(projectCoachObject)
                }
            }
        }
        
        return projectsCoach
    }
    
    /**
    Converts an JSON object to a Workspace.
    
    :param: JSON     The Json to convert.
    
    :returns: An array of Workspace objects.
    */
    public func ConvertToWorkspaces(jsonData: JSON) -> [Workspace]{
        var workspaces = [Workspace]()
        if jsonData != nil {
            for workspace in jsonData.arrayValue {
                
                if let workspaceId = workspace["id"].string {
                    println(workspaceId)
                    let workspaceObject = Workspace(id: workspaceId)
                    
                    if let workspaceName = workspace["name"].string {
                        workspaceObject.name = workspaceName
                    }
                    
                    if let workspaceURL = workspace["url"].string {
                        workspaceObject.url = workspaceURL
                    }
                    
                    if let workspaceImage = workspace["thumbnail_url"].string {
                        workspaceObject.imageUrl = workspaceImage
                    }
                    
                    if let workspaceUnread = workspace["unread_items"].int {
                        workspaceObject.unreadItems = workspaceUnread
                       // self.workspacesUnreadItems += workspaceUnread
                    }
                    
                    workspaces.append(workspaceObject)
                }
            }
        }
        
        return workspaces
    }
    
    /**
    Converts an JSON object to a Notification.
    
    :param: JSON     The Json to convert.
    
    :returns: An array of Notification objects.
    */
    public func ConvertToNotifications(jsonData: JSON) -> [Notification]{
        var notifications = [Notification]()
        if jsonData != nil {
            for notification in jsonData.arrayValue {
                if let notificationId = notification["id"].string {
                    let notificationObject = Notification(id: notificationId)
                    
                    if let notificationFromUser = notification["from_user"].string {
                        notificationObject.fromUser = notificationFromUser
                    }
                    
                    if let notificationTimeStamp = notification["timestamp"].string {
                        notificationObject.timestamp = notificationTimeStamp
                    }
                    
                    if let notificationType = notification["type"].string {
                        notificationObject.type = notificationType
                    }
                    
                    if let notificationOneliner = notification["text"].string {
                        notificationObject.oneLiner = notificationOneliner
                    }
                    
                    if let notificationUnread = notification["unread"].bool {
                        notificationObject.unread = notificationUnread
                    }
                    
                    if let notificationLocation = notification["location"]["url"].string {
                        notificationObject.location = notificationLocation
                    }
                    
                    if let thumbnailURL = notification["location"]["thumbnail_url"].string {
                        notificationObject.thumbnailUrl = thumbnailURL
                    }
                    
                    if let name = notification["location"]["name"].string {
                        notificationObject.name = name
                    }
                    
                    notifications.append(notificationObject)
                }
            }
        }
        return notifications
    }
}