//
//  config.swift
//  Projectcampus
//
//  Created by Jeroen Zonneveld on 07-05-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit

/**
Global config for Projectcampus app. Settings will be
*/
public struct Config {
    
    /** 
        Links to the Projectcampus website
    */
    public struct Links {
        
//        /// The main URL
//        public static let Url = "https://test.projectcamp.us"
//        
//        /// The base API url to Projectcampus
//        public static let Base = "https://api-test.projectcamp.us"
//        
//        /// The URL where you can find the config
//        public static let Settings = "https://api-test.projectcamp.us/config"
//        
//        /// The URL to call when the authorization process is done
//        public static let CallBack = "oauth-swift://oauth-callback/projectcampus"
//        
//        /// The URL where users can register
//        public static let Register = "http://api.projectcamp.us/register"
        
        /// The main URL
        public static let Url = "https://projectcamp.us"
        
        /// The base API url to Projectcampus
        public static let Base = "https://api.projectcamp.us"
        
        /// The URL where you can find the config
        public static let Settings = "https://api.projectcamp.us/config"
        
        /// The URL to call when the authorization process is done
        public static let CallBack = "oauth-swift://oauth-callback/projectcampus"
        
        /// The URL where users can register
        public static let Register = "http://api.projectcamp.us/register"
        
    }
    
    /**
        All the settings that are needed to set up a oAuth 2.0 connection with the Projectcampus API. 
    */
    public struct Authorization {
        
        /// The URL what you will need to connect to the Projectcampus API.
        public static var AuthorizationUrl:String?
        
        /// The Client ID what you will need to connect to the Projectcampus API.
        public static var ClientId:String?
        
        /// The Client Secret what you will need to connect to the Projectcampus API.
        public static var ClientSecret:String?
        
        /// The Token URL what you will need to connect to the Projectcampus API.
        public static var TokenUrl:String?
        
        /// The response type what you will need to connect to the Projectcampus API.
        public static let ResponseType:String = "code"
    }
    
    /**
        Color settings
    */
    public struct Color {
        
        /// Color settings for the header
        public struct Header {
        
            /// The Navigation header background color
            public static var Background = UIColor (red:0.153, green:0.671, blue:0.831, alpha:1)
            
            /// The Navigation header text color
            public static var Text = UIColor.whiteColor()
        }
        
        /// Color settings for the header
        public struct Notification {
            
            /// The Notification read background color
            public static var Read = UIColor.whiteColor()
            
            /// The Notification unread background color
            public static var Unread = UIColor(red: 0.863, green:0.937, blue:0.957, alpha:1)
            
            /// The Notification text color
            public static var Text = UIColor.whiteColor()
        }
        
        // Color for the table views
        public struct TableView {
            // The color on select a cell
            public static var TableBlueOnClick = UIColor(red: 0.549, green:0.827, blue:0.882, alpha:1)
        }
        
    }
    
    public static let UserAgent:String = "Projectcampus"
    
}