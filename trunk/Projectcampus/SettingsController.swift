//
//  TableViewController.swift
//  Projectcampus
//
//  Created by Kwok Hey Wong on 23/04/15.
//  Copyright (c) 2015 Kwok Hey Wong. All rights reserved.
//

import UIKit

/// The controller for the settings
class SettingsController: UITableViewController {
   let opties = ["Notifications", "Profile settings", "Mail accounts", "External accounts", "Logout"]

    /// The outlet for the tableview
    @IBOutlet var tableViewOutlet: UITableView!

    /// Initialises the settings controller view
    override func viewDidLoad() {
        tableView.backgroundColor = UIColor(red:0.957, green:0.984, blue:0.992, alpha:1) /*#f4fbfd*/
        tableView.tableFooterView = UIView(frame:CGRectZero)
        
        tableViewOutlet.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        tableViewOutlet.separatorInset = UIEdgeInsetsZero
        
        tableViewOutlet.scrollEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Sets the amount of cells to create
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return opties.count
    }
    
    /// Initialises the cells for the settings tableview
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SettingsCell", forIndexPath: indexPath) as! SettingsCell
        
        var varCell = opties[indexPath.item]
        var image: UIImage = UIImage(named: varCell)!
        cell.imageOutlet.image = image
        cell.labelOutlet.text = varCell
        cell.id = indexPath.item
        
        return cell
    }
    
    /// Segues to the settingsWebviewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "instellingenWebview" {
            if let dvc = segue.destinationViewController as? settingsWebViewController {
                if let buttonPressed = sender as? SettingsCell {
                    switch buttonPressed.id {
                    case 0:
                        dvc.nameOfPage = "Notification settings"
                        dvc.urlForWebview = "/settings/notifications"
                    case 1:
                        dvc.nameOfPage = "Profile settings"
                        dvc.urlForWebview = "/settings/profile"
                    case 2:
                        dvc.nameOfPage = "Mail accounts"
                        dvc.urlForWebview = "/settings/emails"
                    case 3:
                        dvc.nameOfPage = "External accounts"
                        dvc.urlForWebview = "/settings/accounts"
                    case 4:
                        dvc.logout = true
                        
                    default:
                        //Nothing
                        break
                    }
                }
            }
        }
    
    }
    
    ///Sets the seperator line between the cells
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
    }
    
    ///Sets the highlight color of the cell
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! SettingsCell
        selectedCell.backgroundColor = Config.Color.TableView.TableBlueOnClick.colorWithAlphaComponent(0.1)
    }
    
    ///Resets the color of the cell
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! SettingsCell
        selectedCell.backgroundColor = UIColor.whiteColor()
    }
    
}
