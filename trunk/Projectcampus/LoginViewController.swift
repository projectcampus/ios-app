//
//  LoginViewController.swift
//  Projectcampus
//
//  Created by Jeroen Zonneveld on 21-04-15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit

/// The view controller for the login
class LoginViewController : UIViewController {
    
    /**
        Function to start the autorisation in safari
    */
    @IBAction func getAuthorization(sender: UIButton) {
        Authorisation.SharedInstance.logout()
        var connection = Authorisation.SharedInstance
    }

    /**
        Function to register as a new Projectcampus user. Will go to Safari.
    */
    @IBAction func registerButton(sender: UIButton) {
        if let checkURL = NSURL(string: Config.Links.Register) {
            if UIApplication.sharedApplication().openURL(checkURL) {
                println("url successfully opened")
            }
        } else {
            println("invalid url")
        }
    }
    
    /**
        Disable auto rotate on login screen
    */
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    /**
        Disable auto rotate on login screen
    */
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientation.Portrait.rawValue
    }

    
}


