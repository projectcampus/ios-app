//
//  SettingsCell.swift
//  Projectcampus
//
//  Created by Kwok Hey Wong on 13/05/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import UIKit

/// The custom cell for the settings tableview
class SettingsCell: UITableViewCell {

    /// The outlet for the UI ImageView
    @IBOutlet var imageOutlet: UIImageView!
    
    /// The label for the settingscell
    @IBOutlet var labelOutlet: UILabel!
    var id = Int()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
