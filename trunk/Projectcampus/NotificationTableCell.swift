//
//  NotificationTableCell.swift
//  Projectcampus
//
//  Created by Johan van der Meulen on 12/05/15.
//  Copyright (c) 2015 Jeroen Zonneveld. All rights reserved.
//

import Foundation
import UIKit

/// The custom cell for the notification tableview
class NotificationTableCell: UITableViewCell  {
    
    /// The outlet for the icon displayed in the cell
    @IBOutlet weak var IconOutlet: UIImageView!
    
    /// The oneliner text which explaines the notification
    @IBOutlet weak var oneLinerTextOutlet: UILabel!
    
    /// The outlet to display the timestamp
    @IBOutlet weak var timeStampOutlet: UILabel!
    
    /// The id of this notification
    var id = Int()
        
}