# Projectcampus
Projectcampus is an app for iOS where you can find al your courses and projects.

The Projectcampus app uses the [Projectcampus API][Projectcampus API] to get the data.  

### Requirements

* iOS 8 or later

### App Store

To install Projectcampus app, go to the App Store on your iOS Device or download the app in iTunes.

[Projectcampus API]: https://docs.projectcamp.us/api "Projectcampus API"
